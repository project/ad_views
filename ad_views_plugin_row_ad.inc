<?php
/*
 * Implementation of ad_views_row_plugin
 */

class ad_views_plugin_row_ad extends views_plugin_row {

  function options(&$options) {
    $options['ad_display'] = 'raw';
  }

  function options_form(&$form, &$form_state) {
    $form['ad_display'] = array(
      '#title' => t('Ad Display'),
      '#desription' => t('Display method to use.'),
      '#type' => 'radios',
      '#default_value' => isset($this->options['ad_display']) ? $this->options['ad_display'] : 'raw',
      '#options' => array(
        'raw' => t('Raw'),
        'iframe' => t('Iframe'),
        'jquery' => t('JQuery'),
        'javascript' => t('Javascript'),
        'default' => t('Default')
      )
    );
  }
}
